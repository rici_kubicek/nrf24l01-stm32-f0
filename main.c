

#include <stdint.h>
#include <system_stm32f0xx.h>
#include <stm32f0xx_gpio.h>
#include <stm32f0xx_rcc.h>
#include <uart.h>
#include <nRF24L01.h>
#include <stm32f0xx_exti.h>
#include <stm32f0xx_misc.h>
#include <stm32f0xx_syscfg.h>


#define RX_PAYLOAD           18   // nRF24L01 TX payload length
#define TX_PAYLOAD           18   // nRF24L01 TX payload length

volatile uint8_t buf[TX_PAYLOAD];

//uint8_t buf[RX_PAYLOAD];
uint8_t have_data;

static uint8_t CRC7_one(uint8_t t, uint8_t data) {
	const uint8_t g = 0x89;
	uint8_t i;

	t ^= data;
	for (i = 0; i < 8; i++) {
		if (t & 0x80) t ^= g;
		t <<= 1;
	}

	return t;
}

uint8_t CRC7_buf(const uint8_t * p, uint8_t len) {
	uint8_t j,crc = 0;

	for (j = 0; j < len; j++) crc = CRC7_one(crc,p[j]);

	return crc >> 1;
}

void EXTI0_1_IRQHandler(void) {
	uint8_t i;
	uint8_t status;

	UART_SendStr("---IRQ-->>>\n");
	if (EXTI_GetITStatus(EXTI_Line0) != RESET) {
		for (i = 0; i < RX_PAYLOAD; i++) buf[i] = 0x00;
		UART_SendStr("RXPacket: ");
		status = nRF24_RXPacket(buf,RX_PAYLOAD);
		nRF24_ClearIRQFlags();
		UART_SendHex8(status);

		if (status == 0x0E) {
			UART_SendStr(" => FIFO Empty (fake alarm)\n");
		} else {
			have_data = 1;
			UART_SendChar('\n');
		}

//		GPIOC->ODR ^= GPIO_Pin_9; // Toggle LED

		nRF24_RXMode(RX_PAYLOAD);
		nRF24_ClearIRQFlags();

		EXTI_ClearITPendingBit(EXTI_Line0);
	}

	UART_SendStr("---IRQ--<<<\n");
}


//void assert_failed(uint8_t* file, uint32_t line)
//{
//	UART_SendStr("\r\n\r\nbug na line: ");
//	UART_SendInt(line);
//	UART_SendStr("\r\n\r\n");
//	while(1);
//}


int main(void)
{


	// Initialises the system clock
	SystemInit();
//	SystemCoreClockUpdate();

	UART_Init(115200);
	UART_SendStr("\nSTM32F030F4P6 is online.\n\r");
	nRF24_init();

	// Enables the clock for GPIOC
//	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA,ENABLE);

	uint8_t trala[2];
	trala[0]=32;
	trala[1]=95;


//	uint8_t buf[TX_PAYLOAD]; // nRF24L01 payload buffer

	// Configures the GPIOC pin8 and pin9, since leds are connected
	// to PC8 and PC9 of GPIOC
//	InitGpio.GPIO_Pin = (GPIO_Pin_5 | GPIO_Pin_4);
//	InitGpio.GPIO_Mode = GPIO_Mode_OUT;
//	InitGpio.GPIO_Speed = GPIO_Speed_Level_1;
//	InitGpio.GPIO_OType = GPIO_OType_PP;
//	InitGpio.GPIO_PuPd = GPIO_PuPd_NOPULL;

	// Initialises the GPIOC
//	GPIO_Init(GPIOA, &InitGpio);


	// Turn ON the Leds
//	GPIO_SetBits(GPIOA, (GPIO_Pin_5 | GPIO_Pin_4));
//	UART_SendStr("A\r\n");
	// Delay
//	for (i = 0; i < 0x0FFFFF; i++);

	// Turn OFF the Leds
//	GPIO_ResetBits(GPIOA, (GPIO_Pin_5 | GPIO_Pin_4));
//	UART_SendStr("B\r\n");
	// Delay
//	for (i = 0; i < 0x0FFFFF; i++);






//	Delay_ms(500);


	uint8_t txbuf[5] = { 0xA9,0xA9,0xA9,0xA9,0xA9 };
	uint8_t rxbuf[5];
	uint8_t i;

	nRF24_WriteBuf(nRF24_CMD_WREG | nRF24_REG_TX_ADDR,txbuf,5); // Write fake TX address
	nRF24_ReadBuf(nRF24_REG_TX_ADDR,rxbuf,5); // Try to read TX_ADDR register
//	for (i = 0; i < 5; i++) if (rxbuf[i] != txbuf[i]) return 1;

	uint8_t o,p;

//		nRF24_WriteBuf(nRF24_CMD_WREG | nRF24_REG_TX_ADDR,txbuf,5); // Write fake TX address
//		nRF24_RWReg(nRF24_CMD_WREG | nRF24_REG_TX_ADDR,0xA8);
//		nRF24_RWReg(nRF24_CMD_WREG | nRF24_REG_TX_ADDR,0xA8);
//		nRF24_RWReg(nRF24_CMD_WREG | nRF24_REG_TX_ADDR,0xA8);
//		nRF24_RWReg(nRF24_CMD_WREG | nRF24_REG_TX_ADDR,0xA8);
//		nRF24_RWReg(nRF24_CMD_WREG | nRF24_REG_TX_ADDR,0xA8);

//		nRF24_ReadBuf(nRF24_REG_TX_ADDR,rxbuf,5); // Try to read TX_ADDR register
//		rxbuf[0]=nRF24_ReadReg(nRF24_REG_TX_ADDR);
//		rxbuf[1]=nRF24_ReadReg(nRF24_REG_TX_ADDR);
//		rxbuf[2]=nRF24_ReadReg(nRF24_REG_TX_ADDR);
//		rxbuf[3]=nRF24_ReadReg(nRF24_REG_TX_ADDR);
//		rxbuf[4]=nRF24_ReadReg(nRF24_REG_TX_ADDR);
		o=0;
		if (rxbuf[o] != txbuf[o]) {
			p=1;
		}
		else {
			p=0;
		}
		UART_SendStr("ssdfsf: ");
		UART_SendHex8(p);
		UART_SendStr(" \n\r");
		o++;
		if (rxbuf[o] != txbuf[o]) {
			p=1;
		}
		else {
			p=0;
		}
		UART_SendStr("ssdfsf: ");
		UART_SendHex8(p);
		UART_SendStr(" \n\r");
		o++;
		if (rxbuf[o] != txbuf[o]) {
			p=1;
		}
		else {
			p=0;
		}
		UART_SendStr("ssdfsf: ");
		UART_SendHex8(p);
		UART_SendStr(" \n\r");
		o++;
		if (rxbuf[o] != txbuf[o]) {
			p=1;
		}
		else {
			p=0;
		}
		UART_SendStr("ssdfsf: ");
		UART_SendHex8(p);
		UART_SendStr(" \n\r");
		o++;
		if (rxbuf[o] != txbuf[o]) {
			p=1;
		}
		else {
			p=0;
		}
		UART_SendStr("ssdfsf: ");
		UART_SendHex8(p);
		UART_SendStr(" \n\r");
		o++;
		if (rxbuf[o] != txbuf[o]) {
			p=1;
		}
		else {
			p=0;
		}

//while(1);
//	nRF24_Check();

//	if (nRF24_Check() != 0) {
//	 UART_SendStr("Got wrong answer from SPI device.\n");
//		UART_SendStr("MCU is now halt.\n");
//		while(1);
//	}
		nRF24_TXMode();
		nRF24_Wake();
	unsigned char v;

	for (i = 0; i < 0x1D; i++) {
		UART_SendHex8(i);
		UART_SendStr("=");
		v = nRF24_ReadReg(i);
		UART_SendHex8(v);
		UART_SendStr("  ");
	}
	UART_SendChar('\n');

//	nRF24_RXMode(RX_PAYLOAD);

	uint16_t pejsek,k,fds,cntr;
	fds=374;
	cntr=498;

	// Prepare data packet
	        buf[0]  = (uint8_t)(fds >> 8); // Temperature
	        buf[1]  = (uint8_t)fds;
	        buf[2]  = (uint8_t)(cntr >> 24); // Iterations counter
	        buf[3]  = (uint8_t)(cntr >> 16);
	        buf[4]  = (uint8_t)(cntr >> 8);
	        buf[5]  = (uint8_t)cntr;
	        buf[6]  = (uint8_t)(fds >> 8); // Vcc
	        buf[7]  = (uint8_t)fds;
	        buf[8]  = (uint8_t)fds; // RAW Time
	        buf[9]  = (uint8_t)fds;
	        buf[10] = (uint8_t)fds;
	        buf[11] = (uint8_t)fds; // RAW Date
	        buf[12] = (uint8_t)fds;
	        buf[13] = (uint8_t)fds;
//	        buf[14] = (uint8_t)(cntr >> 8); // LSI frequency
//	        buf[15] = (uint8_t)cntr;
	        // buf[16] will be filled later with OBSERVER_TX register value
	        buf[TX_PAYLOAD - 1] = CRC7_buf(&buf[0],TX_PAYLOAD - 1); // Put packet CRC byte
	        UART_SendStr(" Buf=[");
	        	                UART_SendBufHex((char*)&buf[0],TX_PAYLOAD);
	        	                UART_SendStr("]");
	        	                nRF24_Wake();Delay_ms(5);nRF24_TXMode();

while(1){
//	nRF24_TXMode();
//			nRF24_Wake();
	        // Send packet
	                i = nRF24_TXPacket(buf,TX_PAYLOAD);
//	                UART_SendStr(" Buf=[");
//	                UART_SendBufHex((char*)&buf[0],TX_PAYLOAD);
	                UART_SendStr("TX=");
	                UART_SendHex8(i);


	                // Put OBSERVER_TX value into buffer for next iteration
	                i = nRF24_ReadReg(nRF24_REG_OBSERVE_TX);
	                buf[16] = i;

	                UART_SendStr(" OBSERVE_TX=");
	                UART_SendHex8(i >> 4); UART_SendChar(':');
	                UART_SendHex8(i & 0x0F); UART_SendChar('\n');
	                nRF24_ClearIRQFlags();
	                Delay_ms(1000);
}


//	nRF24_TXPacket(trala,18);
//	for(k=0;k<2;k++)
//		{
//			UART_SendStr("\r\nReseult: ");
//			pejsek=nRF24_TXPacket(trala,18);
//			UART_SendHex16(pejsek);
//			UART_SendStr("\r\n");
//		}

	nRF24_ClearIRQFlags();

//	// Enable Alternative function (for EXTI)
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);

	 /* Enable SYSCFG clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	 /* Connect EXTI0 Line to PA0 pin */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);

	// EXTI pin
//	GPIO_EXTILineConfig(GPIOA,GPIO_PinSource0);

	// Configure EXTI line1
	EXTI_InitTypeDef EXTIInit;
	EXTIInit.EXTI_Line = EXTI_Line0;             // EXTI will be on line 0
	EXTIInit.EXTI_LineCmd = ENABLE;               // EXTI1 enabled
	EXTIInit.EXTI_Mode = EXTI_Mode_Interrupt;     // Generate IRQ
	EXTIInit.EXTI_Trigger = EXTI_Trigger_Falling; // IRQ on signal falling
	EXTI_Init(&EXTIInit);

	// Configure EXTI1 interrupt
	NVIC_InitTypeDef NVICInit;
	NVICInit.NVIC_IRQChannel = EXTI0_1_IRQn;
	NVICInit.NVIC_IRQChannelCmd = ENABLE;
	NVICInit.NVIC_IRQChannelPriority = 0;
	NVIC_Init(&NVICInit);

	int16_t temp;
	uint8_t CRC7;
	uint16_t vrefint;
	uint16_t LSI_freq;
	uint8_t TR1, TR2, TR3;
	uint8_t DR1, DR2, DR3;
	uint8_t hours, minutes, seconds;
	uint8_t day, month, year;

	have_data = 0;
	while(1) {
		while(!have_data);

		have_data = 0;
		CRC7 = CRC7_buf(&buf[0],RX_PAYLOAD-1);

		UART_SendStr("RX=["); UART_SendBufHex((char*)&buf[0],RX_PAYLOAD);
		UART_SendStr("] CRC=("); UART_SendInt(buf[RX_PAYLOAD-1]);
		UART_SendStr("|"); UART_SendInt(CRC7);
		UART_SendStr((buf[RX_PAYLOAD-1] == CRC7) ? ") => ok\n" : ") => bad\n");
		UART_SendStr("Temperature: ");
		temp = (buf[0] << 8) | buf[1];
		UART_SendInt(temp / 10); UART_SendChar('.');
		temp %= 10;
		if (temp < 0) temp *= -1;
		UART_SendInt(temp % 10); UART_SendStr("C\n");

		UART_SendStr("Packet: #");
		UART_SendInt((uint32_t)((buf[2] << 24)|(buf[3] << 16)|(buf[4] << 8)|(buf[5])));
		UART_SendChar('\n');

		vrefint = (buf[6] << 8) + buf[7];
		UART_SendStr("Vcc: ");
		UART_SendInt(vrefint / 100);
		UART_SendChar('.');
		UART_SendInt0(vrefint % 100);
		UART_SendStr("V\n");

		LSI_freq = (buf[14] << 8) + buf[15];
		UART_SendStr("LSI: ");
		UART_SendInt(LSI_freq);
		UART_SendStr("Hz\n");

		UART_SendStr("OBSERVE_TX:\n  ");
		UART_SendHex8(buf[16] >> 4); UART_SendStr(" pckts lost\n  ");
		UART_SendHex8(buf[16] & 0x0F); UART_SendStr(" retries\n");

		TR1 = buf[8];
		TR2 = buf[9];
		TR3 = buf[10];
		DR1 = buf[11];
		DR2 = buf[12];
		DR3 = buf[13];
		seconds = ((TR1 >> 4) * 10) + (TR1 & 0x0F);
		minutes = ((TR2 >> 4) * 10) + (TR2 & 0x0F);
		hours   = (((TR3 & 0x30) >> 4) * 10) + (TR3 & 0x0F);
	    day   = ((DR1 >> 4) * 10) + (DR1 & 0x0F);
	    //dow   = DR2 >> 5;
	    month = (((DR2 & 0x1F) >> 4) * 10) + (DR2 & 0x0F);
	    year  = ((DR3 >> 4) * 10) + (DR3 & 0x0F);

		UART_SendStr("Uptime: ");
        UART_SendInt0(hours); UART_SendChar(':');
        UART_SendInt0(minutes); UART_SendChar(':');
        UART_SendInt0(seconds); UART_SendChar(' ');
        UART_SendInt0(day); UART_SendChar('.');
        UART_SendInt0(month); UART_SendStr(".20");
        UART_SendInt0(year); UART_SendChar('\n');
	}






}
